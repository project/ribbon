Notifications Ribbon

-- SUMMARY --

Top Notifications Bar shown on every page of our Drupal 7 site.

-- REQUIREMENTS --

*  Jquery Colorpicker.

-- INSTALLATION --

Extract ribbon.zip in '/sites/all/modules' directory and activate it through drupal admin

-- FEATURES --

* Background Color Customizable.
* Font Color Customizable.
* Allow Background Transparent.
* ( Show | Hide ) Close Button.
* Convert the Text on Marquee.

-- CONTACT --

Maintainers:

* Panagiotis Adamopoulos <p.adamopoulos@dnacreative.gr>
