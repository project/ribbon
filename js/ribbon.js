(function ($) {

    $().ready(function () {

        if (Drupal.settings.Ribbon.Marquee.enabled)
            $('#ribbon-marquee')
                .marquee(Drupal.settings.Ribbon.Marquee);

        $('#ribbon-command').click(function () {
            $.cookie('Drupal.Ribbon.Hidden', 1, {path: '/'});
            $('#ribbon-container').slideUp();
        });

    });

})(jQuery);